# Local message

This is a notifications plugin for moodle. It will display notifications to users and allows admins to create them.


## Functionality
- Form for admins to add new notification
- Store read messages for users, do not display twice.
- Store notifications in database.
