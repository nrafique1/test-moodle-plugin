<?php

/**
 * @package     local_message
 * @author      Nishad
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @var stdClass $plugin
 */

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'local_message';
$plugin->version = 2021020800;
$plugin->requires = 2016052300; // Moodle version
